var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var models = require('../models');

passport.use(new LocalStrategy(
  	function(username, password, done) {
	    models.User.findOne({ 
	    	where: {
	    		username: username 
	    	}
	    }).then(user => {
	      	if (!user.validPassword(password)) { return done(null, false, { message: "Nom d'utilisateur ou mot de passe incorrect." }); }
	      	return done(null, user);
	    }).catch(err => {
	      	return done(null, false, { message: "Nom d'utilisateur ou mot de passe incorrect." });
	    });
  	}
));

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	models.User.findById(id)
	.then(user => {
		done(null, user);
	}).catch(err => {
		console.log(err);
	})
});

/* GET home page. */
router.get('/', function(req, res, next) {
  	res.render('index', {user: req.user});
});

router.get('/login', function(req, res, next) {
	if(req.user) {
		res.redirect('/account');
	} else {
		res.render('login');
	}
});

router.post('/login', passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login', failureFlash: true, successFlash: 'Bienvenue !' }), function(req, res) {
    res.redirect('/');
});

router.get('/logout', function(req, res, next) {
	req.session.destroy(function (err) {
    	res.redirect('/');
  	});
});

module.exports = router;
