var express = require('express');
var paginate = require('express-paginate');
var models = require('../models');
var router = express.Router();

var app = express();

/* GET users listing. */
router.get('/', function(req, res, next) {
	models.Car
	.findAndCountAll({
		offset: req.skip,
     	limit: req.query.limit,
     	include: [
     		models.Brand
     	]
	})
	.then(result => {
    	var pageCount = Math.ceil(result.count / req.query.limit);
    	var resultCount = result.count;
    	var resultValues = result.rows;
    	var currentPage = req.param('page');

    	var brands = models.Brand.findAll()
    	.then(result => {
    		res.render('Car/cars', {
	        	cars: resultValues,
	        	pageCount,
	        	resultCount,
	        	currentPage, 
	        	pages: paginate.getArrayPages(req)(3, pageCount, req.query.page),
	        	brands: result,
	        	user: req.user,
	      	});
    	});
  	});
});

router.get('/:carId/delete', function(req, res, next) {
	var carId = req.params.carId;
	models.Car.findById(carId).then(function(car) {
		car.destroy();
		req.flash('success', "La voiture a bien été supprimé.");
		res.redirect('/cars');
	});
});

router.post('/', function(req, res, next) {
	var carParams = req.body;
	if(carParams['car[name]'] && carParams['car[brandId]']) {
		console.log(carParams['car[brandId]']);
		models.Brand.findById(carParams['car[brandId]'])
		.then(result => {
			models.Car.create({
				name: carParams['car[name]'],
				brandId: result.id,
			}).then(result => {
				req.flash('success', "La voiture a bien été ajouté.");
				res.redirect('/cars');
			}).catch(err => {
				req.flash('error', err);
				console.log(err);
				res.redirect('/cars');
			});
		}).catch(err => {
			req.flash('error', err);
			console.log(err);
			res.redirect('/cars');
		});
	}
});

module.exports = router;
