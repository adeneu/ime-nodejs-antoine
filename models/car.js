'use strict';
var validator = require('validator');
var models = require('../models');

module.exports = (sequelize, DataTypes) => {
  var Car = sequelize.define('Car', {
    name: {
      type: DataTypes.STRING,
    },
    brandId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: { 
        model: "brands", 
        key: "id" 
      }
    },
  });

  Car.associate = function(models) {
    models.Car.belongsTo(models.Brand, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false,
      }
    });
  };
  return Car;
};