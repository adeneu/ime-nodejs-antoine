'use strict';
var validator = require('validator');
var models = require('../models');

module.exports = (sequelize, DataTypes) => {
  var Message = sequelize.define('Message', {
    message: {
      type: DataTypes.TEXT,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: { 
        model: "Users", 
        key: "id" 
      }
    },
  });
  return Message;
};