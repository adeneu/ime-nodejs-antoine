'use strict';
var validator = require('validator');
var bcrypt = require('bcrypt');
var models = require('../models');

module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('User', {
        username: {
            type: DataTypes.STRING,
            unique: true,
            validate: {
                notEmpty: { arg: true, msg: 'Ce champ ne doit pas être vide.' },
                //unique: { arg: true, msg: 'CetCe champ ne doit pas être vide.' }
            }
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
            validate: {
                //unique: { arg: true, msg: 'Cette adresse mail existe déjà.' },
                isEmail: { arg: true, msg: 'Vous devez saisir une adresse e-mail valide.' },
                notEmpty: { arg: true, msg: 'Ce champ ne doit pas être vide.' }
            }
        },
        password: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: { arg: true, msg: 'Ce champ ne doit pas être vide.' }
            }
        },
        salt: {
            type: DataTypes.STRING,
        }
    }, {

    });

    User.prototype.validPassword = function(password) {
        return bcrypt.compareSync(password, this.password);
    };

    User.beforeCreate(function(user, options) {
        user.salt = bcrypt.genSaltSync(8);
        user.password = bcrypt.hashSync(user.password, salt);
        return true;
    });
    return User;
};