'use strict';
var validator = require('validator');
var models = require('../models');

module.exports = (sequelize, DataTypes) => {
  var Brand = sequelize.define('Brand', {
    name: {
      type: DataTypes.STRING,
    },
  });

  Brand.associate = function(models) {
    models.Brand.hasMany(models.Car);
  };

  return Brand;
};