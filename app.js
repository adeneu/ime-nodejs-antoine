var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var session = require('express-session');
var validator = require('validator');
var paginate = require('express-paginate');
var passport = require('passport');

var index = require('./routes/index');
var users = require('./routes/users');
var cars = require('./routes/cars');
var brands = require('./routes/brands');
var chatApp = require('./routes/chatApp');

var app = express();

app.use(session({ 
	maxAge: 86400,
	secret: 'tata',
    name: 'express_session_cookie',
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

app.use(flash());

app.use(paginate.middleware(5, 5));

app.use(function(req, res, next){
    res.locals.success = req.flash('success');
    res.locals.errors = req.flash('error');
    next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session({
	maxAge: 86400,
	secret: 'tata',
    name: 'express_session_cookie',
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

app.use('/', index);
app.use('/users', users);
app.use('/cars', cars);
app.use('/brands', brands);
app.use('/chat', chatApp);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
