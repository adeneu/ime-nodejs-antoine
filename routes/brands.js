var express = require('express');
var paginate = require('express-paginate');
var models = require('../models');
var router = express.Router();

var app = express();

/* GET users listing. */
router.get('/', function(req, res, next) {
	models.Brand
	.findAndCountAll({
		offset: req.skip,
     	limit: req.query.limit,
     	include: [
     		models.Car
     	]
	})
	.then(result => {
    	var pageCount = Math.ceil(result.count / req.query.limit);
    	var resultCount = result.count;
    	var resultValues = result.rows;
    	var currentPage = req.param('page');

		res.render('Brand/brands', {
        	brands: resultValues,
        	pageCount,
        	resultCount,
        	currentPage, 
        	pages: paginate.getArrayPages(req)(3, pageCount, req.query.page),
        	user: req.user
      	});
  	});
});

router.get('/create', function(req, res, next) {
	res.render('Brand/brands_create');
});

router.post('/create', function(req, res, next) {
	var brandParams = req.body;
	if(brandParams['brand[name]']) {
		models.Brand.create({
			name: brandParams['brand[name]'],
		}).then(result => {
			req.flash('success', "La marque a bien été ajouté.");
			res.redirect('/brands');
		}).catch(err => {
			req.flash('error', err);
			console.log(err);
			res.redirect('/brands');
		});
	} else {
		console.log(brandParams['brand[name]']);
	}
});


router.get('/:brandId/delete', function(req, res, next) {
	var brandId = req.params.brandId;
	models.Brand.findById(brandId).then(function(brand) {
		brand.destroy();
		req.flash('success', "La marque a bien été supprimé.");
		res.redirect('/brands');
	});
});

router.get('/:brandId/view', function(req, res, next) {
	var brandId = req.params.brandId;
	models.Brand.find({
		where: {
			id: brandId
		}
	}).then(result => {
		brand = result;
		models.Car.findAndCountAll({
			offset: req.skip,
	     	limit: req.query.limit,
			where: {
				brandId: brandId
			}
		}).then(result => {
			console.log(result);
			var pageCount = Math.ceil(result.count / req.query.limit);
	    	var resultCount = result.count;
	    	var resultValues = result.rows;
	    	var currentPage = req.param('page');

			res.render('Brand/brands_view', {
	        	cars: resultValues,
	 			brand: brand,
	        	pageCount,
	        	resultCount,
	        	currentPage, 
	        	pages: paginate.getArrayPages(req)(3, pageCount, req.query.page),
	        	user: req.user,
	      	});
		});
	});
});

module.exports = router;
