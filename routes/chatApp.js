var express = require('express');
var router = express.Router();
var models = require('../models');

var http = require('http').Server(express);
var io = require('socket.io')(http);

router.get('/', function(req, res, next) {
	models.Message
	.findAll()
	.then(result => {
		res.render('chat', {messages: result});
	});
});

io.on('connection', function(socket){
	socket.broadcast.emit('chat message' ,"Un nouvel utilisateur vient de se connecter.");
	io.emit('chat message', "Bonjour et bienvenue sur notre chat");
  	socket.on('chat message', function(msg){
    	models.Message
    	.create({
    		message: msg,
    		userId: 49,
    	})
    	.then(chatmsg => {
    		io.emit('chat message', msg);
    	})
    	.catch(err => {
    		console.log(err);
    	})
  	});
});

http.listen(2000, function(){
  	console.log('listening on *:2000');
});

module.exports = router;