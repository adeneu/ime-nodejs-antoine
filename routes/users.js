var express = require('express');
var paginate = require('express-paginate');
var models = require('../models');
var router = express.Router();

var app = express();

/* GET users listing. */
router.get('/', function(req, res, next) {
	if(req.user) {
		models.User
		.findAndCountAll({
			offset: req.skip,
	     	limit: req.query.limit
		})
		.then(result => {
	    	var pageCount = Math.ceil(result.count / req.query.limit);
	    	var resultCount = result.count;
	    	var resultValues = result.rows;
	    	var currentPage = req.param('page');
	    	res.render('User/users', {
	        	users: resultValues,
	        	pageCount,
	        	resultCount,
	        	currentPage, 
	        	pages: paginate.getArrayPages(req)(3, pageCount, req.query.page),
	        	user: req.user
	      	});
	  	});
	} else {
		res.redirect('/login')
	}
});

router.post('/', function(req, res, next) {
	if(req.user) {
		var userParams = req.body;
		console.log(userParams);
		models.User.create({
			username: userParams['user']['username'],
			email: userParams['user']['email'],
			password: userParams['user']['password'],
		}).then(result => {
			console.log(result);
			req.flash('success', "L'utilisateur a bien été ajouté.");
			res.redirect('/users');
		}).catch(err => {
			req.flash('error', err.message);
			res.redirect('/users');
		});
	} else {
		res.redirect('/login');
	}
});

router.get('/:userId/delete', function(req, res, next) {
	var userId = req.params.userId;
	models.User.findById(userId).then(function(user) {
		user.destroy();
		req.flash('success', "L'utilisateur a bien été supprimé.");
		res.redirect('/users');
	});
});

module.exports = router;
